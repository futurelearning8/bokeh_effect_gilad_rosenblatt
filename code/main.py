import cv2
import numpy as np
import warnings


class BokehEffect:

    def __init__(self, filename, threshold=30, stupid_but_works=True):
        """
        Initialize Bokeh effect filter object.

        :param str filename: path to video file to operate on.
        :param int threshold: value used to decide what to blur in each frame (compared to distance from median frame).
        """

        # Save path to video file.
        self.filename = filename

        # Threshold to use
        self.threshold = threshold

        # Save masking technique flag.
        self.stupid_but_works = stupid_but_works

        # Initialize the center point variable.
        self.center_point = (np.nan, np.nan)

        # Initialize video dimensions and frame of median values across the ENTIRE video.
        self.number_of_frames = None
        self.frame_width = None
        self.frame_height = None
        self.reference_frame = None

        # Populate the dimensions and reference frame state parameters.
        self.get_frame_of_medians()

    def select_center_point_callback(self, event, x, y, flags, param):
        """
        Save (x, y) coordinates for the point the user left-clicked on to instance variable.

        :param event: event type.
        :param int x: x coordinate of clicked point.
        :param int y: y coordinate of clicked point.
        :param flags: ignored.
        :param param: ignored.
        """
        if event == cv2.EVENT_LBUTTONDOWN:
            self.center_point = (x, y)
            print(f"User selected point: {self.center_point}.")

    def filter_image(self, frame):
        """
        Blur image according to it distance from the reference (median) image and the threshold state parameter.

        :param frame: BGR frame to filter (8-bit unsigned integers)
        :return tuple: filtered frame and its corresponding mask (indicating pixels at which it was not blurred).
        """

        # If no center point is chosen continue without change.
        x, y = self.center_point
        if np.isnan(x) or np.isnan(y):
            return frame, np.zeros_like(frame)

        # Convert BGR frames to HSV.
        reference_frame_hsv = cv2.cvtColor(self.reference_frame, cv2.COLOR_BGR2HSV)
        target_frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # Compute the distance between reference and target frames along the hue and saturation dimensions of HSV.
        distance_hs = cv2.absdiff(
            reference_frame_hsv[..., 0:2],
            target_frame_hsv[..., 0:2]
        ).sum(axis=2).astype(np.uint8)

        # Generate a mask that marks pixels whose distance is beyond the threshold value.
        _, mask = cv2.threshold(distance_hs, self.threshold, 255, cv2.THRESH_BINARY)

        # Find all connected components in the mask.
        num_components, labels, stats, centroids = cv2.connectedComponentsWithStats(mask, connectivity=8)
        boxes = stats[:, 0:-1]
        areas = stats[:, -1]

        # Calculate mask according to the component(s) that contain the center point.
        if self.stupid_but_works:
            # Slow method: eliminate components whose bounding boxes do not contain the center point (skip background).
            for index in range(1, num_components):
                if not BokehEffect.box_contains_point(boxes[index], (x, y)):
                    mask[labels == index] = 0
        else:
            # "Fast" but not-working-method: limit mask to the single label that (should) contain the center point.
            target_label = labels[x, y]  # FIXME always evaluates to 0 (background label)!
            # mask[labels != target_label] = 0
            mask = (~(labels == target_label) * 255).astype(np.uint8)  # Just mark the background as a compromise :(

        # Extend mask to channel dimension.
        mask = np.tile(mask[:, :, np.newaxis], [1, 1, 3])

        # Combine original frame with a blurred frame according to the mask.
        blurred_frame = cv2.GaussianBlur(frame, (21, 21), 0)
        combined_frame = np.where(mask, frame, blurred_frame)

        # Return filtered frame and its corresponding mask.
        return combined_frame, mask

    def run(self):

        # Open a window and associate a callback with left-button clicks.
        window_name = f"Bokeh effect for {self.filename}"
        cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(window_name, 1800, 900 * self.frame_height // self.frame_width)
        cv2.moveWindow(window_name, 300, 300)
        cv2.setMouseCallback(window_name, self.select_center_point_callback)

        # Start a video capture.
        capture = cv2.VideoCapture(self.filename)

        # Run the video.
        while True:

            # Read a new frame.
            retained, frame = capture.read()
            if not retained:
                # Video ended.
                break

            # Blur image at the right places.
            frame, mask = self.filter_image(frame)

            # Draw the selected center point on the mask
            x, y = self.center_point
            if not np.isnan(x) and not np.isnan(y):
                mask = cv2.circle(mask, (int(x), int(y)), 6, (255, 0, 0), 2)

            # Display the current blurred frame alongside the mask and center point mark.
            cv2.imshow(window_name, np.hstack((frame, mask)))
            if cv2.waitKey(25) & 0xff == ord("q"):
                break

        # Release capture and destroy window.
        capture.release()
        cv2.destroyWindow(window_name)

    def get_frame_of_medians(self):
        """Calculate "frame" of **MEAN** (faster) value for each channel in each pixel across the **ENTIRE** video."""

        # Start a video capture.
        capture = cv2.VideoCapture(self.filename)

        # Save video file dimensions.
        self.number_of_frames = int(capture.get(cv2.CAP_PROP_FRAME_COUNT))
        self.frame_width = int(capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.frame_height = int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        number_of_channels = 3

        # Initialize an empty array of proper dimensions for video content.
        video = np.empty(
            shape=(self.number_of_frames, self.frame_height, self.frame_width, number_of_channels),
            dtype=np.dtype(np.uint8)
        )

        # Iterate over all frames and save video to array (verify expected number of frames matches actual number).
        for index in range(self.number_of_frames + 1):

            # Read a new frame (break if video ended).
            retrieved, frame = capture.read()

            # Verify there were no mistakes in estimating number of frames using cv2.CAP_PROP_FRAME_COUNT.
            if index == self.number_of_frames and retrieved:
                warnings.warn("Mismatch between estimated and actual number of frames: missed the last few frames..")
            if index < self.number_of_frames and not retrieved:
                warnings.warn("Mismatch between estimated and actual number of frames: left blank space.")
            if not retrieved or index == self.number_of_frames:
                # Video ended or array is full.
                break

            # Assign new frame to array at legal index.
            video[index] = frame

        # Release video capture.
        capture.release()

        # Calculate and save a "frame" comprising of median value for each pixel across all frames in the video.
        self.reference_frame = np.mean(video, axis=0).astype(np.uint8)  # FIXME use np.median instead of np.mean.

    @staticmethod
    def box_contains_point(box, point):
        """
        Checks whether input point is located inside the input box.

        :param box: bounding box (x, y, w, h) where x, y are the top-left coordinate and w, h are the width and height.
        :param point: point (x0, y0)
        :return bool: True if points is inside the box (False otherwise).
        """
        return box[0] < point[0] < box[0] + box[2] and box[1] < point[1] < box[1] + box[3]


def main():
    filenames = {
        "woman": "../videos/woman_on_floor.mp4",
        "car": "../videos/car_with_balloons.mp4",
        "dorian": "../videos/dorian.mp4"
    }
    BokehEffect(
        filename=filenames["car"],
        threshold=30,
        stupid_but_works=False  # Stupid and and slow method that works on "woman" video (too slow for "car" video).
    ).run()


if __name__ == "__main__":
    main()
