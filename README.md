# Bokeh Effect

This is the Bokeh Effect exercise for FL8 week 4 day 1 by Gilad Rosenblatt.

## Instructions

### Run
Run `main.py` to run the Bokeh Effect filter one of the videos (can be selected by changing a dictionary entry).

### Data
Videos should be in a `videos` folder (not included in repo) while the code is in the `code` folder.

## Results
Code works with one problem:

1. Using one option of finding the connected component(s) containing the user-selected center point leads to very good results but is horribly slow. It looks for all label bounding boxes that contain the center point.

2. Using the "standard" method of searching for the label that contains the selected (x, y) center point with simple indexing does not work. It always evaluates to the background label (0) and I do not know why (for now).

## License

[WTFPL](http://www.wtfpl.net/)
